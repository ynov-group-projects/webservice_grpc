import React, {useContext} from 'react';
import {GlobalContext} from "./Context";

const MessageList = (Props) => {
    const {currentUser} = Props;
    const {messages} = useContext(GlobalContext);
    return (
        <ul className="message-list">
            {messages?.map((message, index) => {
                return (
                    <li key={message.id} className={`message ${currentUser === message.user? "message-fromUser" : ""}`}>
                        <div>
                            <span>{message.user}</span>
                        </div>
                        <div>
                            <p>
                                {message.text}
                            </p>
                        </div>
                    </li>
                );
            })}
        </ul>
    );
}

export default MessageList;