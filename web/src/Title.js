import React, {useState} from 'react';

const Title = ({onSubmitName}) => {
    const [submit, setSubmit] = useState(false)
    const [name, setName] = useState("")

    const handleChange = (e) => {
        setName(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setSubmit(true);
        onSubmitName(name);
    }

    return (
        <div className="title">
            {submit ? (<p>{name}</p>) :
                (<form
                    onSubmit={(e) => handleSubmit(e)}
                    className="send-message-form">
                    <label>Vôtre Pseudo: </label>
                    <input
                        onChange={(e) => handleChange(e)}
                        autoFocus={true}
                        value={name}
                        placeholder="Ecrivez vôtre pseudo et valider avec ENTRER"
                        type="text"/>
                </form>)}
        </div>
    );
}

export default Title;