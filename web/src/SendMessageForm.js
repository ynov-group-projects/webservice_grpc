import React, {useState} from "react";

const SendMessageForm = ({sendMessage}) => {
    const [message, setMessage] = useState("")

    const handleChange = (e) => {
        setMessage(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        sendMessage(handleCommand(message))
        setMessage("")
    }

    const handleCommand = (message) => {
        if(message.startsWith("/random")) {
            let args = message.split(" ");
            message = `My random number is ${randomIntFromInterval(args[1], args[2])}`
        }
        return message;
    }

    const randomIntFromInterval = (min, max) => { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    return (
        <form
            onSubmit={(e) => handleSubmit(e)}
            className="send-message-form">
            <input
                onChange={(e) => handleChange(e)}
                value={message}
                autoFocus={true}
                placeholder="Ecrire le message et envoyer avec ENTRER"
                type="text"/>
        </form>
    )
}

export default SendMessageForm;